#include "linkedList.h"


/*
The funciton creats a new node according to the given information
Input: the value to put inside the node
Output: a pointer to the created node
*/
node* createNode(unsigned int value)
{
	node* newNode = new node;

	newNode->value = value;
	newNode->next = NULL;

	return newNode;
}


/*
The function inserts the given node at the start of the given linked list
Input: the head of the linked list, and the node to insert at the start
*/
void insertAtBegging(node** head, node* newHead)
{
	newHead->next = (*head);  //changes the "next" element in the newHead to point to the start of the linked list - "head"
	(*head) = newHead;  //changes the first node of the list to the given node.
}


/*
The function deletes the first node of the given linked list
Input: the head of the linked list
*/
void deleteFirst(node** head)
{
	if (*head)
	{
		node* wantedNode = (*head);  //saves the first node in the list - the head
		(*head) = (*head)->next;  //releases the first node of the list
		delete wantedNode;
	}
}


/*
Function will free all memory of the persons list
input: a linked list
*/
void deleteList(node** head)
{
	node* temp = NULL;
	node* curr = *head;

	while (curr) // when curr == NULL, that is the end of the list, and loop will end
	{
		temp = curr;
		curr = (curr)->next;
		delete(temp);
	}

	*head = NULL;
}

