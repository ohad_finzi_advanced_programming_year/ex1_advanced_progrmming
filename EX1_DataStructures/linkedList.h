#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>

typedef struct node
{
	unsigned int value;
	node* next;
}node;

node* createNode(unsigned int value);

void insertAtBegging(node** head, node* newHead);

void deleteFirst(node** head);

void deleteList(node** head);


#endif /* LINKEDLIST */

