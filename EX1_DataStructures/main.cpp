#include <iostream>

#include "queue.h"
#include "linkedList.h"
#include "stack.h"
#include "utils.h"


// MEMORY LEAK CHECK
//#define _CRTDBG_MAP_ALLOC 
//#include <crtdbg.h>


void checkQueueFunctions();
void checkLinkedListFunctions();
void checkStackFunctions();
void checkReverse(); 
void checkReverse10();


int main()
{
	//checkQueueFunctions();

	//checkLinkedListFunctions();

	//checkStackFunctions();

	//checkReverse();

	//checkReverse10();
	
	return 0;
}


/*
The function shows how i checkes the all the queue functinos from "queue.h" in part1 questions
*/
void checkQueueFunctions()
{
	int i = 0;

	queue* q = new queue;

	initQueue(q, 5);  //cretes a queue in the max size of 5 elements
	enqueue(q, 1);  //filling the queue
	enqueue(q, 2);
	enqueue(q, 3);
	enqueue(q, 4);
	enqueue(q, 5);

	for (i = 0; i < q->_size + 1; i++)  //should print all the queue elements
	{
		printf("Elment taken: %d\n", dequeue(q));
	}

	cleanQueue(q);
	delete q;

	//printf("Leaks: %d", _CrtDumpMemoryLeaks());  // To check memory leak 
}


/*
The function shows how i checkes the all the linkedList functinos from "linkedList.h" in part2 question1
*/
void checkLinkedListFunctions()
{
	node* head = createNode(5);
	node* node1 = createNode(6);
	node* node2 = createNode(7);
	node* node3 = createNode(8);

	insertAtBegging(&head, node1);
	insertAtBegging(&head, node2);
	insertAtBegging(&head, node3);

	node* curr = head;
	while (curr)
	{
		printf("%d\n", curr->value);
		curr = curr->next;
	}

	deleteFirst(&head);
	deleteFirst(&head);

	curr = head;
	while (curr)
	{
		printf("%d\n", curr->value);
		curr = curr->next;
	}

	deleteList(&head);

	//printf("Leaks: %d", _CrtDumpMemoryLeaks());  // To check memory leak 
}


/*
The function shows how i checkes the all the stack functinos from "stack.h" in part2 question2
*/
void checkStackFunctions()
{
	stack* s = new stack;

	initStack(s);
	push(s, 1);
	push(s, 2);
	push(s, 3);
	push(s, 4);
	push(s, 5);

	/*
	while (s->myStack)
	{
		printf("%d\n", pop(s));
	}
	*/

	node* curr = s->myStack;
	while (curr)
	{
		printf("%d\n", curr->value);
		curr = curr->next;
	}

	printf("%d\n", pop(s));
	push(s, 5);
	printf("%d\n", pop(s));

	cleanStack(s);
	delete s;

	//printf("Leaks: %d", _CrtDumpMemoryLeaks());  // MEMORY LEAK CHECK
}



/*
The function shows how i checked the function "void reverse(int* nums, unsigned int size)" in part2 question3
*/
void checkReverse()
{
	int nums[] = { 1,2,3,4 };
	int size = (sizeof(nums) / sizeof(*nums));  //gets the array length
	int i = 0;

	for (i = 0; i < size; i++)  //prints the array before reverse
	{
		printf("%d, ", nums[i]);
	}

	reverse(nums, size);

	for (i = 0; i < size; i++)  //prints the array after the reverse
	{
		printf("%d, ", nums[i]);
	}

	//printf("Leaks: %d", _CrtDumpMemoryLeaks());  // MEMORY LEAK CHECK
}


/*
The function shows how i checked te function"int* reverse10()" in part2 question4
*/
void checkReverse10()
{
	int i = 0;
	int* ary = reverse10();

	for (i = 0; i < 10; i++)
	{
		printf("%d, ", ary[i]);
	}

	delete[] ary;

	//printf("Leaks: %d", _CrtDumpMemoryLeaks());  // MEMORY LEAK CHECK
}

