#include "queue.h"


/*
The function creates the queue by allocating the dynamic memory for the array according to the given size
Input: The queue to create and the wanted size
*/
void initQueue(queue* q, unsigned int size)
{
	q->_elements = new unsigned int[size];  //allocates dynamic memory to create the elements array of the queue
	q->_size = size;
	q->_counter = 0;
}


/*
The function clears the given queue by deleting the allocated dynamic memory in its element array
Input: The queue to clear
*/
void cleanQueue(queue* q)
{
	delete [] q->_elements;  //clears the allocated dynamic memory used for the elements array
	q->_counter = 0;
	q->_size = 0;
}


/*
The function inserts a new element to the given queue
Input: The queue to add the new element and the new element
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (q->_counter < q->_size)  //checks if the given queue isnt full 
	{
		q->_elements[q->_counter] = newValue;  //inserts the given value in the queue
		q->_counter++;
	}
}


/*
The function releases the first "in line" value of the given queue
Input: The queue to get the first element from
Output: If the given queue is empty the function will return -1, but if not it will return the first index of element array of the queue
*/
int dequeue(queue* q)
{
	int returnVal = -1;
	int i = 0;

	if (q->_counter > 0)
	{
		returnVal = q->_elements[0];  //gets the first entered value in the given queue

		for (i = 0; i < q->_counter - 1; i++)  //the loop moves all the queue elements one place back
		{
			q->_elements[i] = q->_elements[i + 1];
		}
		q->_elements[i] = NULL;  //resets the last element

		q->_counter--;  //lowers counter after taking a value from the queue
	}

	return returnVal;
}

