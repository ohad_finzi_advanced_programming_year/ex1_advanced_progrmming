#include "stack.h"


/*
The function adds a new node to the stack
Input: the stack to add the new node to, and the element of the node
*/
void push(stack* s, unsigned int element)
{
	if (s->counter == 0)  //checks if the stack is empty
	{
		s->myStack->value = element;  //if the stack is empty the head node will hold the given element
	}
	else
	{
		node* newNode = createNode(element);  //creats a new node
		insertAtBegging(&(s->myStack), newNode);  //and inserts it at the begging of the stack
	}

	s->counter++;  //afater adding a new node it add +1 to the node counter
}


/*
The function returns the element of the head node of the stack's linked list and deletes it
Input: the stack
Output: the head node element of the stack if existed, and -1 if not
*/
int pop(stack* s)
{
	int returnVal = -1;

	if (s->myStack)  //checks if the stack isnt empty
	{
		returnVal = s->myStack->value;  //if the stack isnt empty, the return value will be the head node's value
		deleteFirst(&s->myStack);  //deletes the head element

		s->counter--;  //lowers the number of node in the stack after deleting the head node
	}

	return returnVal;
}


/*
The funciton creates a new stack by creating a new linked list
Input: the stack
*/
void initStack(stack* s)
{
	s->myStack = new node; 
	s->counter = 0;
	s->myStack->next = NULL;
	s->myStack->value = NULL;
}


/*
The function deletes all the linked list created for the stack using the linked list functions
Input: the stack to delete
*/
void cleanStack(stack* s)
{
	deleteList(&(s->myStack));
}

