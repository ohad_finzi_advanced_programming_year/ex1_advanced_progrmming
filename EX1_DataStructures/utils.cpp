#include "utils.h"


/*
The function reverses the given array element from start to end using the stack ADT
Input: a pointer to the array to reverse and the size of it
*/
void reverse(int* nums, unsigned int size)
{
	int i = 0;

	stack* s = new stack;  //creates a new stack
	initStack(s);

	for (i = 0; i < size; i++)  //inserts all the array elements inside the stack using the push funtion
	{
		push(s, nums[i]);
	}

	for (i = 0; i < size; i++)  //inserts all the stack element (the array elements but reversed) inside the array using the pop function
	{
		nums[i] = pop(s);
	}

	cleanStack(s);  //deletes the dynamic memory allocated for the creation of the stack
	delete s;
}


/*
The funtion creates a dynamic array and filled its element with numbers given by the user, and returns the array with its numbers in a reversed order
Output: a dynamic array which its elements are in the reversed order of what the user entered
*/
int* reverse10()
{
	int i = 0;
	int size = 10;
	int* ary = new int[size];

	std:: cout << "Please enter 10 numbers: \n";
	for (i = 0; i < size; i++)  
	{
		std:: cin >> ary[i];  
	}

	reverse(ary, size);

	return ary;
}

